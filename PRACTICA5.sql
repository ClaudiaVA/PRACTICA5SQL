CREATE DATABASE PRACTICA5

CREATE TABLE Personas(
persona_id SMALLINT PRIMARY KEY NOT NULL,
 Nombre varchar(50)NOT NULL,
 Apellido varchar(50)NULL,
 Genero CHAR(1) NOT NULL, 
 Pais VARCHAR(50) NULL)



CREATE TABLE Libro(
	libro_id SMALLINT NOT NULL,
	titulo VARCHAR(50) NOT NULL,
	precio DECIMAL(5,2) NULL,
	persona_id SMALLINT  NULL,
	PRIMARY KEY (libro_id),
	FOREIGN KEY(persona_id) REFERENCES Personas (persona_id));




	/* llenar datos a las Tablas*/
	INSERT INTO Personas 
 VALUES(1,'Maria','Rodriguez', 'F','Bolivia'),
       (2,'Pedro','Marquez', 'M','Ecuador'),
       (3,'Ali','Perez', 'M','Colombia'),
       (4,'Juan','Loayza', 'M','Argentina'),
       (5,'Fernanda','Mendez', 'F','Peru'),
       (6,'Lucero','Alba', 'F','Chile'),
       (7,'Luis','Smith', 'M','EEUU'),
	   (8,'Antonia','Sanchez', 'F','Singapur'),
	   (9,'Francisco','Montas', 'M','Bolivia'),
	   (10,'Claudia','Villarroel', 'F','Bolivia'),
	   (11,'Juan Jose','Garcia', 'M','Bolivia'),
       (12,'Jaime','Rocha', 'M','Bolivia'),
       (13,'Aurelio','Valdez', 'M','Italia'),
       (14,'Ana','Sousa', 'M','Brasil'),
       (15,'Julio','Quiroga', 'M','Peru');
       


	   INSERT INTO Libro
 VALUES(50,'Las mil y una noches','50', 1),
       (51,'La Odisea','150',10),
       (52,'La Iliada','160',2),
       (53,'Aladino','30',11),
       (54,'La Biblia','80',3),
       (55,'El Coran','80', 12),
       (56,'Metafisica','90', 10),
       (57,'Arpas Eternas','200', 4),
       (58,'Origenes Adanicos','350', 5),
       (59,'Sabes lo que crees?','160',6),
	   (60,'Mi planta de naranjaLima','120',11),
	   (61,'Historia de Bolivia','400',12),
	   (62,'Juan Salvador Gaviota','45',10),
	   (63,'Cien anios de Soledad','270',11),
	   (64,'Arpas Eternas II','200',12),
	   (65,'Arpas Eternas II',NULL,NULL);
	   


	   SELECT *  FROM Personas

	   /* PRACTICA 5 sql */


	   /*
	   Integrantes: Claudia Villarroel A.
                    Juan Jose Garcia
                    Jaime Rocha
	   */



     /* Example 1*/

	   CREATE FUNCTION  ListadoPais (@country varchar (20))
	   returns @Clientes table
	   (Nombre varchar (20),Apellido varchar (20),pais varchar(20))
	  
	   as
	   begin
	   Insert @Clientes select Nombre,Apellido, pais from Personas where pais= @country
	   Return
	   end
	       
	   Select * from dbo.ListadoPais ('Bolivia')




	   /*Example 2*/

	   -- Function to display the cheapest book price as well as the average book price
      SELECT MIN(precio) as Cheapeast_price, AVG(precio) as Average_book_price
      FROM Libro




	   /*Example 3*/


	   CREATE FUNCTION Reduction
       (@reduction int) 

		RETURNS DECIMAL(4,2)
		AS BEGIN
			DECLARE @redAmount decimal(4,2);
			DECLARE @genre char(1);

			IF @genre = 'F'
				SET @redAmount = (@redAmount * @reduction) / 100;
			ELSE
				SET @redAmount = @redAmount;
			RETURN @redAmount;
		 END;