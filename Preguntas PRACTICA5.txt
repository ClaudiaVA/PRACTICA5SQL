*** Integrantes: Claudia Villarroel A.
                 Juan Jose Garcia
                 Jaime Rocha

                                   * PRACTICA 5 *



** Averiguar si es necesario colocar NOT NULL cuando se usa PRIMARY KEY **


It's possible to create a table without in need to use NOT NULL when declaring a Primary Key


CREATE TABLE Biblioteca (
    Biblioteca_Id int PRIMARY KEY,
    Name varchar(255) NOT NULL,
    Street varchar(255),
    City int,
    Libro_id int
 )





** Cuando es recomendable usar el tipo de dato MONEY y DECIMAL **



In Money and SMALLMONEY fields, you can not handle more than 4 decimals

If an object is defined as money, it can have a maximum of 19 digits and 4 of them can be to the right of the decimal. The object uses 8 bytes to store the data. Therefore, the money data type has a precision of 19, a scale of 4 and a length of 8.

The money and smallmoney data types are limited to four decimal places. Use the decimal data type if more decimal spaces are needed.

Use a point to separate the partial units of currency, such as cents, from the complete units of currency. For example, 2.15 can specify 2 dollars and 15 cents.

Coma separators are not allowed in the money or smallmoney constants, although the presentation format of these types of data includes them.


The DECIMAL data type (p, s) is designed to contain decimal numbers. When the user specifies a column of this type, he writes his precision (p) as the total number of digits he can store, between 1 and 32. He also writes his scale scale (s) as the number of those digits left on the right of the decimal point.









** Hay alguna diferencia entre los tipos de datos DECIMAL  y  NUMERIC **

They are the same for almost all purposes.

At one time different vendors used different names (Numeric/Decimal) for almost the same thing. SQL-92 made them the same with one minor difference which can be vendor specific:

NUMERIC must be exactly as precise as it is defined � so if you define 4 decimal places, the DB must always store 4 decimal places.

DECIMAL must be at least as precise as it is defined. This means that the database can actually store more digits than specified (due to the behind-the-scenes storage having space for extra digits). This means the database might store 1.00005 instead of 1.0000, affecting future calculations.

In SQL Server Numeric is defined as being identical to Decimal in every way � both will always store only the specified number of decimal places.




Investigar sobre  FUNCTIONS  en  SQL server y desarrollar 3 ejemplos.



